//
// main.c
// finspect
//
// Copyright (c) 2013, Adam Knight
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions 
// are met:
// 
// 1. Redistributions of source code must retain the above copyright 
// notice, this list of conditions and the following disclaimer.
// 
// 2. Redistributions in binary form must reproduce the above copyright 
// notice, this list of conditions and the following disclaimer in the 
// documentation and/or other materials provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE 
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
// POSSIBILITY OF SUCH DAMAGE.

#include "memdmp/memdmp.h"  // memdmp

#include <stdlib.h>         // uint* malloc free
#include <stdbool.h>        // true/false
#include <fcntl.h>          // open/read/close
#include <sys/stat.h>       // stat
#include <sys/mman.h>       // mmap
#include <errno.h>          // errno + codes
#include <libgen.h>         // basename
#include <getopt.h>         // getopt, getopt_long
#include <math.h>           // log2, floor
#include <string.h>         // strlcpy
#include <sys/param.h>      // MIN/MAX

#ifdef DEBUG
#define debugf(format, ...) { char _s[1024]; sprintf(_s, format, __VA_ARGS__); fprintf(stderr, "%s\n", _s); }
#define debug(msg)          fprintf(stderr, "%s\n", msg)
#else
#define debugf(format, ...) ;;
#define debug(msg) ;;
#endif

typedef unsigned char Byte;

static char PROGRAM[PATH_MAX];

typedef struct _search_opts {
    char*  search_string;
    Byte   needle[256];
    size_t nsize;
    size_t before;
    size_t after;
} search_opts;

#pragma mark Interface

bool  isPowerOfTwo(double x);
int   minbytes(uint64_t v);
void* _memmem(const void * _haystack, size_t haystack_size, const void * _needle, size_t needle_size);
void  buf_search(const Byte* haystack, size_t hsize, const search_opts* s_opts, memdmp_opts opts, memdmp_state *search_state);
void  syserr(char* msg, int err, int rval) __attribute__((noreturn));
void  usage(int rval) __attribute__((noreturn));

#pragma mark Implementation

int main (int argc, char* const argv[])
{
    strlcpy(PROGRAM, argv[0], PATH_MAX);
    
    // Display configuration
    memdmp_opts opts = {
        .base = 16,
        .group = {
            .width = 4,
            .count = 4,
        },
        .display = {
            .color = true,
            .address = false,
            .offset = true,
            .encoded = true,
            .text = true,
            .padding = true,
            .truncate = true,
        }
    };
    
    search_opts s_opts = { .after = 512 };
    
    size_t nbytes = 0;
    off_t offset = 0;
    
    /* options descriptor */
    struct option longopts[] = {
        { "nbytes",         required_argument,  NULL,                   'n' },
        { "offset",         required_argument,  NULL,                   'o' },
        { "base",           required_argument,  NULL,                   'b' },
        { "width",          required_argument,  NULL,                   'w' },
        { "count",          required_argument,  NULL,                   'c' },
        
        { "search",         required_argument,  NULL,                   's' },
        { "after",          required_argument,  NULL,                   'A' },
        { "before",         required_argument,  NULL,                   'B' },
        { "length",         required_argument,  NULL,                   'l' },

        { "padding",        no_argument,        &opts.display.padding,  1 },
        { "no-padding",     no_argument,        &opts.display.padding,  0 },
        
        { "color",          no_argument,        &opts.display.color,    1 },
        { "no-color",       no_argument,        &opts.display.color,    0 },
        
        { "text",           no_argument,        &opts.display.text,     1 },
        { "no-text",        no_argument,        &opts.display.text,     0 },
        
        { "help",           no_argument,        NULL,                   'h' },
        
        { NULL,             0,                  NULL,                   0 }
    };

    int ch;
    while ((ch = getopt_long(argc, argv, "n:o:b:w:c:s:A:B:l:h", longopts, NULL)) != -1)
        switch (ch) {
            case 's':
                s_opts.search_string = strdup(optarg);
                break;
            
            case 'n':
                sscanf(optarg, "%zi", &nbytes);
                break;
            
            case 'o':
                sscanf(optarg, "%zi", &offset);
                break;
        
            case 'A':
                sscanf(optarg, "%zi", &s_opts.after);
                break;
        
            case 'B':
                sscanf(optarg, "%zi", &s_opts.before);
                break;

            case 'b':
                sscanf(optarg, "%hhi", &opts.base);
                if (opts.base < 2 || opts.base > 36) {
                    char msg[100];
                    sprintf(msg, "unsupported base: %u", opts.base);
                    syserr(msg, EINVAL, 1);
                }
                break;

            case 'w':
                sscanf(optarg, "%hhi", &opts.group.width);
                if (opts.group.width < 1 || ! isPowerOfTwo(opts.group.width) ) opts.group.width = 4;
                break;

            case 'c':
                sscanf(optarg, "%hhi", &opts.group.count);
                if (opts.group.count < 1) opts.group.count = 4;
                break;
            
            case 0:
                // long opt, save from the default case
                break;

            default:
                usage(1);
        }
    
    argc -= optind;
    argv += optind;
    
    if (argc) {
        // The file to read.
        const char* path = argv[argc-1];
        
        // Verify the file.
        struct stat s = {0};
        if (stat(path, &s) < 0) syserr((char*)path, 0, 1);
        
        // Verify the read params.
        if (s.st_size && nbytes == 0) nbytes = s.st_size;
        if ( offset + nbytes && s.st_size) {
            if ( (offset + nbytes) > s.st_size ) {
                // The user is pointing too far into the file. See if we can truncate the result.
                if (offset < s.st_size) {
                    nbytes = (s.st_size - offset);
                    // fprintf(stderr, "adjusted nbytes to %zu", nbytes);
                } else {
                    debugf("file size: %llu", s.st_size);
                    syserr("offset too large for file", EINVAL, 1);
                }
            }
        }
                
        // Open the file
        int fd = open(path, O_RDONLY);
        if (fd < 0) syserr((char*)path, 0, 1);
        
        // Prepare the search, if specified
        if (s_opts.search_string != NULL) {
            debugf("Searching for '%s'", s_opts.search_string);
            char* next = s_opts.search_string;
            while ( (next[0] != '\0') ) {
                uint64_t x = 0;
                char* string = calloc(1, 1024);
                int pos = 0;
                size_t num_bytes = 0;
                
                if (sscanf(next, "%lli%n", &x, &pos)) {
                    // This trick DEPENDS on x being in little endian format.
                    // If this gets ported to a BE system, find another trick.
                    num_bytes = minbytes(x);
                    memcpy( (s_opts.needle + s_opts.nsize), &x, num_bytes );
                    s_opts.nsize += num_bytes;
                    
                } else if (sscanf(next + pos, "%s%n", string, &pos)) {
                    num_bytes = strlen(string);
                    // debugf("Read %u chars", num_bytes);
                    memcpy( (s_opts.needle + s_opts.nsize), string, num_bytes );
                    s_opts.nsize += num_bytes;
                }
                next += pos;
                free(string);
                
                // FIXME: We probably want to do this sooner...
                if (s_opts.nsize > sizeof(s_opts.needle)) break;
            }
        }
        
        // Begin.
        memdmp_state state = { .offset = offset };
        Byte* buf = mmap(NULL, nbytes, PROT_READ, MAP_PRIVATE, fd, 0);
        if (buf == MAP_FAILED) {
            debugf("mmap: %s", strerror(errno));
            // Fall back to manual reading and accounting (ungh)
            
            size_t default_size = 4*1024*1024;
            size_t bytes_read = 0, total_read = 0, read_size = 0;
            
            memdmp_state search_state = { .prev = "5432" };
            read_size = (nbytes ? MIN(nbytes, default_size) : default_size);
            buf = calloc(1, read_size);
            
            printf("Reading %zu bytes, starting at %zd.\n", read_size, offset);
            
            if ( lseek(fd, offset, SEEK_SET) < 0 ) syserr("lseek", 0, 1);
            while ( (bytes_read = read(fd, buf, read_size)) > 0 ) {
                size_t buf_size = MIN( bytes_read, read_size );
                
                if (s_opts.nsize > 0)
                    buf_search(buf, buf_size, &s_opts, opts, &search_state);
                else
                    memdmp(stdout, buf, buf_size, &opts, &state);
                
                // debugf("read: %zu bytes of %zu", total_read, nbytes);
                if (nbytes) {
                    total_read += bytes_read;
                    if (total_read >= nbytes) break;
                } else {
                    if (bytes_read == 0) break;
                }
            }
            free(buf);
            
        } else {
            debug("using mmap");
            Byte* haystack = (buf+offset);
            memdmp_state search_state = {0};
            
            if (s_opts.nsize > 0)
                buf_search((buf+offset), nbytes, &s_opts, opts, &search_state);
            else
                memdmp(stdout, haystack, nbytes, &opts, &state);
            
            munmap(buf, nbytes);
        }
        
        close(fd);
        
    } else {
        usage(1);
    }
    
    return 0;
}

bool isPowerOfTwo(double x)
{
    double y = log2(x);
    return (y == floor(y));
}

int minbytes(uint64_t v)
{
    int length = 1;
    if ((v & 0xFFFFFFFF00000000) != 0)
    {
        length += 4;
        v >>= 32;
    }
    if ((v & 0xFFFF0000) != 0)
    {
        length += 2;
        v >>= 16;
    }
    if ((v & 0xFF00) != 0)
    {
        length += 1;
        v >>= 8;
    }
    return length;
}

void* _memmem(const void * _haystack, size_t haystack_size, const void * _needle, size_t needle_size)
{
    const Byte * haystack = (Byte*)_haystack;
    const Byte * needle = (Byte*)_needle;
    
    const Byte* cursor = haystack;
    Byte testChar = needle[0];

#define _POS (cursor - haystack)
#define _REM (haystack_size - _POS)
    
    while ( _POS < haystack_size ) {
        // debugf("Position: %zu/%zu", _POS, haystack_size);
        
        // Get the next matching tail character.
        if ( (cursor = memchr(cursor, testChar, _REM)) == NULL )
             break;
        
        // Test for the pattern.
        if ( memcmp(cursor, needle, needle_size) == 0 )
            return (void*)cursor;
        
        // Move up a length.
        cursor += 1;
    }
    
    return NULL;
}

void buf_search(const Byte* haystack, size_t hsize, const search_opts* s_opts, memdmp_opts opts, memdmp_state *search_state)
{
    Byte const * cursor = haystack;
    ssize_t size_remaining = hsize;
    
    while(cursor != NULL) {
        cursor = _memmem(cursor, size_remaining, s_opts->needle, s_opts->nsize);
        if (cursor == NULL) break;
        search_state->offset = (cursor - haystack);
        {
            const Byte* buf = cursor - s_opts->before;
            size_t len = MIN(s_opts->after, size_remaining);
            fprintf(stdout, "\n# offset %#llx\n", search_state->offset);
            memdmp(stdout, buf, len, &opts, search_state);
        }
        size_remaining = hsize - (cursor - haystack) - 1;
        cursor++;
    }
}

void syserr(char* msg, int err, int rval)
{
    if (err > 0) errno = err;
    if (errno && msg) perror(msg);
    if (errno && !msg) perror(PROGRAM);
    if (!errno && msg) fprintf(stderr, "%s: %s\n", PROGRAM, msg);
    if (!errno && !msg) fprintf(stderr, "exit(%d)\n", rval);
    exit(rval);
}

void usage(int rval)
{
    char usage[] =
        "usage: %s [opts] file\n"
        "\n"
        "Numeric arguments can be given in decimal, hexadecimal (0x##), or octal (0##) formats.\n"
        "\n"
        "DISPLAY:\n"
        "   -n, --nbytes        how many bytes of input to process (default: all)\n"
        "   -o, --offset        the offset of the input to start at (default: 0)\n"
        "   -b, --base          which base to use in the dump (default: 16)\n"
        "   -w, --width         how many bytes of input constitute a group (default: 4)\n"
        "   -c, --count         how many groups to a line (default: 4)\n"
        "       --(no-)padding  adds a space after every unit of output that represents a byte of input\n"
        "       --(no-)color    use color in the output to help distinguish values from zero values\n"
        "       --(no-)text     include a textual representation of the values to the right of the encoded values\n"
        "\n"
        "SEARCH:\n"
        "   -s, --search        space-delimited list of up to 256 byte values to search for (eg. \"3000 0xff 0800 0 0 1 WORD\")\n"
        "   -B, --before        number of bytes to print before the result (default: 0)\n"
        "   -A, --after         number of bytes to print after the result (default: 512)\n"
        ;
    fprintf(stderr, usage, PROGRAM);
    exit(rval);
}
