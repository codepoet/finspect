MAKEDEPEND = $(CC) -MM $(CPPFLAGS) -o $*.P $<
CFLAGS = -Wall -Weverything -Wno-sign-conversion -Wno-sign-compare -Wno-padded -Wno-missing-field-initializers -I. -O3 -g
# CFLAGS += -DDEBUG -O1
BINARIES = finspect
SRCS = memdmp/output.c memdmp/memdmp.c main.c
OBJS = memdmp/output.o memdmp/memdmp.o main.o

all: finspect

%.o: %.c
	$(MAKEDEPEND)
	$(COMPILE.c) -o $@ $<

clean:
	$(RM) $(SRCS:.c=.P) $(OBJS) $(BINARIES)

finspect: $(OBJS)
	$(CC) -o $@ $(OBJS)

-include $(SRCS:.c=.P)

.PHONY: all clean

